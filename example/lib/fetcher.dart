import 'package:flutter_common/flutter_common.dart';

class Fetcher extends IFetcher {
  static Fetcher? _instance;

  Fetcher._();

  Fetcher() {
    _instance ??= Fetcher._();
  }

  @override
  Uri getBackendEndpoint(String path) {
    return _generateBackEndEndpoint(path);
  }

  // private:
  Uri _generateBackEndEndpoint(String path) {
    return Uri.parse('https://api.fastocloud.com$path');
  }
}
