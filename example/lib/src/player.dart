import 'package:flutter/material.dart';
import 'package:flutter_webrtc_player/flutter_webrtc_player.dart';

class Player extends StatefulWidget {
  final RTCPlayerController controller;

  const Player(this.controller);

  @override
  _PlayerState createState() => _PlayerState();
}

class _PlayerState extends State<Player> {
  @override
  Widget build(BuildContext context) {
    return WebRTCPlayer(
        controller: widget.controller,
        placeholder: Container(
            color: Colors.black,
            child: const Center(child: Icon(Icons.warning, color: Colors.white))));
  }
}
