import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_common/widgets.dart';
import 'package:flutter_webrtc_player_example/src/p2p/bloc.dart';

class CallIdField extends StatelessWidget {
  const CallIdField();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<P2PCallCubit, CallState>(builder: (_, state) {
      if (state is CallIdState) {
        return TextFieldEx.readOnly(init: state.id, hint: 'Stream id');
      }
      return const SizedBox();
    });
  }
}
