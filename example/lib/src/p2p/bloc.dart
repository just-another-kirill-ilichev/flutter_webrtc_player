import 'dart:async';

import 'package:flutter_webrtc_player/flutter_webrtc_player.dart';
import 'package:flutter_webrtc_player_example/src/bloc.dart';
import 'package:objectid/objectid.dart';

abstract class CallState {
  const CallState();
}

class CallIdState extends CallState {
  final String id;

  const CallIdState(this.id);
}

class CallInitState extends CallState {
  const CallInitState();
}

class CallLoadingState extends CallState {
  const CallLoadingState();
}

class CallRefreshState extends CallState {
  const CallRefreshState();
}

class CallEnterMember extends CallState {
  final String ssid;
  final String sid;

  CallEnterMember(this.ssid, this.sid);
}

class CallLeaveMember extends CallState {
  final String ssid;
  final String sid;

  CallLeaveMember(this.ssid, this.sid);
}

class P2PCallCubit extends BasePlayerCubit<CallState> {
  late CallRTCPlayerController _playerController = CallRTCPlayerController(
      onEnterClient: _enterClientCallback,
      onLeaveClient: _leaveClientCallback,
      onSessionInfo: _sessionInfoCallback);

  final List<RTCPlayerController> controllers = [];

  @override
  CallRTCPlayerController get controller => _playerController;

  P2PCallCubit() : super(const CallInitState()) {}

  void initialize(bool audio, bool video) async {
    final String id = ObjectId().hexString;
    emit(const CallLoadingState());
    late StreamSubscription<RTCPlayerState> sub;
    sub = _playerController.state.listen((event) {
      if (event is PlayingRTCState) {
        emit(CallIdState(id));
        sub.cancel();
      }
    });
    await _playerController.connect('wss://api.fastocloud.com/webrtc_in', id);
    _playerController.initialize(audio, video, 1280, 720);
  }

  void enterSession(String ssid) {
    _playerController.enterSession(ssid);
  }

  void leaveSession(String ssid) {
    _playerController.leaveSession(ssid);
   _clean();
    emit(CallRefreshState());
  }

  void getSessionInfo(String ssid) {
    _playerController.getSessionInfo(ssid);
  }

  void bye() {
    _playerController.bye();
    _clean();
    emit(CallRefreshState());
  }

  void _clean() {
    for (int i = 0; i < controllers.length; ++i) {
      var cont = controllers[i];
      controllers.remove(cont);
      cont.dispose();
    }
  }

  void _enterClientCallback(String ssid, String sid) {
    for (int i = 0; i < controllers.length; ++i) {
      if (controllers[i].id == sid) {
        return;
      }
    }

    var rtc = PlaylistRTCPlayerController();
    rtc.init();
    rtc.connect('wss://api.fastocloud.com/webrtc_out', sid);
    controllers.add(rtc);
    emit(CallEnterMember(ssid, sid));
  }

  void _leaveClientCallback(String ssid, String sid) {
    for (int i = 0; i < controllers.length; ++i) {
      if (controllers[i].id == sid) {
        var cont = controllers[i];
        controllers.remove(cont);
        cont.dispose();
        break;
      }
    }
    emit(CallLeaveMember(ssid, sid));
  }

  void _sessionInfoCallback(String ssid, List<String> sids) {
    for (int i = 0; i < sids.length; ++i) {
      _enterClientCallback(ssid, sids[i]);
    }
  }
}
