import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:flutter_webrtc_player/flutter_webrtc_player.dart';
import 'package:flutter_webrtc_player_example/src/p2p/bloc.dart';

class EnterLeaveSession extends StatelessWidget {
  const EnterLeaveSession();

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<RTCPlayerState>(
        stream: context.read<P2PCallCubit>().controller.state,
        builder: (context, snapshot) {
          final state = snapshot.data;
          if (state is PlayingRTCState) {
            return _EnterLeaveButton(() {
              showDialog(
                  barrierDismissible: false,
                  context: context,
                  builder: (_) => const OptionsDialog()).then((value) {
                if (value != null) {
                  final ssid = value[1];
                  if (value[0]) {
                    context.read<P2PCallCubit>().getSessionInfo(ssid);
                    context.read<P2PCallCubit>().enterSession(ssid);
                  } else {
                    context.read<P2PCallCubit>().leaveSession(ssid);
                  }
                }
              });
            });
          }
          return _EnterLeaveButton(null);
        });
  }
}

class OptionsDialog extends StatefulWidget {
  const OptionsDialog({Key? key}) : super(key: key);

  @override
  _OptionsDialogState createState() => _OptionsDialogState();
}

class _OptionsDialogState extends State<OptionsDialog> {
  String ssid = 'Unknown';

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        title: Text('Options'),
        content: Column(mainAxisSize: MainAxisSize.min, children: [
          TextFieldEx(
              init: ssid,
              onFieldSubmit: (value) {
                setState(() {
                  ssid = value;
                });
              })
        ]),
        actions: [
          TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: const Text('Cancel')),
          ElevatedButton(
              onPressed: () {
                Navigator.of(context).pop([true, ssid]);
              },
              child: const Text('Enter')),
          ElevatedButton(
              onPressed: () {
                Navigator.of(context).pop([false, ssid]);
              },
              child: const Text('Leave'))
        ]);
  }
}

class _EnterLeaveButton extends IconButton {
  static const EdgeInsets _padding = EdgeInsets.symmetric(horizontal: 8);

  _EnterLeaveButton(VoidCallback? onPressed)
      : super(
            icon: const Icon(Icons.arrow_forward_sharp),
            constraints: const BoxConstraints(),
            padding: _padding,
            onPressed: onPressed);
}
