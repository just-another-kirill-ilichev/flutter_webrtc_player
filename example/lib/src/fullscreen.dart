import 'dart:async';
import 'dart:html' hide VoidCallback;
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class Fullscreen {
  static Fullscreen? _instance;
  num? _timeStamp;
  StreamSubscription<Event>? _events;
  final BehaviorSubject<bool> _updates = BehaviorSubject<bool>();

  Fullscreen._();

  factory Fullscreen() {
    if (_instance == null) {
      _instance = Fullscreen._();
      _instance!._updates.add(false);
      _instance!._events = document.onFullscreenChange.listen((event) {
        if (event.timeStamp != _instance!._timeStamp) {
          _instance!._timeStamp = event.timeStamp;
          _instance!._updates.add(!_instance!._updates.value);
        }
      });
    }
    return _instance!;
  }

  Stream<bool> get updates => _updates.stream;

  void setFullscreen(bool value) {
    if (value) {
      document.documentElement!.requestFullscreen();
    } else {
      document.exitFullscreen();
    }
  }

  bool get disposed => _events == null;

  void dispose() {
    if (!disposed) {
      _updates.close();
      _events?.cancel();
    }
  }
}

class FullscreenButton extends StatelessWidget {
  const FullscreenButton();

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<bool>(
        initialData: false,
        stream: Fullscreen().updates,
        builder: (context, snapshot) {
          final bool fullscreenOpened = snapshot.data!;
          if (fullscreenOpened) {
            return _FullscreenButton.close(() {
              Fullscreen().setFullscreen(false);
            });
          } else {
            return _FullscreenButton.open(() {
              Fullscreen().setFullscreen(true);
            });
          }
        });
  }
}

class _FullscreenButton extends IconButton {
  static const EdgeInsets _padding = EdgeInsets.symmetric(horizontal: 8);

  _FullscreenButton.open(VoidCallback onPressed)
      : super(
            icon: const Icon(Icons.fullscreen),
            constraints: const BoxConstraints(),
            padding: _padding,
            onPressed: onPressed);

  _FullscreenButton.close(VoidCallback onPressed)
      : super(
            icon: const Icon(Icons.fullscreen_exit),
            constraints: const BoxConstraints(),
            padding: _padding,
            onPressed: onPressed);
}
