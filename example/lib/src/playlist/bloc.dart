import 'dart:async';
import 'package:flutter_webrtc_player/flutter_webrtc_player.dart';
import 'package:flutter_webrtc_player_example/src/bloc.dart';

class PlayerIdState {
  final String id;

  const PlayerIdState(this.id);
}

class PlayerInitState extends PlayerIdState {
  const PlayerInitState() : super('');
}

class PlayerLoadingState extends PlayerIdState {
  const PlayerLoadingState(String id) : super(id);
}

class PlayerUrlState extends PlayerIdState {
  final String url;

  const PlayerUrlState(this.url, String id) : super(id);
}

class PlaylistCubit extends BasePlayerCubit<PlayerIdState> {
  final PlaylistRTCPlayerController _playerController = PlaylistRTCPlayerController();

  @override
  PlaylistRTCPlayerController get controller => _playerController;

  PlaylistCubit() : super(const PlayerInitState());

  void setId(String id) {
    if (id.isEmpty) {
      emit(const PlayerInitState());
      _playerController.bye();
    } else {
      emit(PlayerLoadingState(id));
      _playerController.connect('wss://api.fastocloud.com/webrtc_out', state.id);
      late StreamSubscription<RTCPlayerState> sub;
      sub = _playerController.state.listen((event) {
        if (event is PlayingRTCState) {
          emit(PlayerIdState(id));
          sub.cancel();
        }
      });
    }
  }
}
