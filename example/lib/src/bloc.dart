import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_webrtc_player/flutter_webrtc_player.dart';

abstract class BasePlayerCubit<T> extends Cubit<T> {
  RTCPlayerController get controller;

  BasePlayerCubit(T init) : super(init) {
    controller.init();
  }

  @override
  Future<void> close() async {
    controller.dispose();
    super.close();
  }
}
