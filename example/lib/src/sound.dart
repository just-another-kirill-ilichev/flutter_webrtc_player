import 'package:flutter/material.dart';
import 'package:flutter_webrtc_player/flutter_webrtc_player.dart';

class RTCPlayerSound extends StatefulWidget {
  final RTCPlayerController controller;

  const RTCPlayerSound(this.controller);

  @override
  _RTCPlayerSoundState createState() => _RTCPlayerSoundState();
}

class _RTCPlayerSoundState extends State<RTCPlayerSound> {
  RTCPlayerController get controller => widget.controller;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<RTCPlayerState>(
        stream: controller.state,
        builder: (context, snapshot) {
          final state = snapshot.data;
          if (state is PlayingRTCState) {
            if (!controller.hasAudio) {
              return const _SoundButton.unmute();
            } else if (controller.muted) {
              return _SoundButton.mute(_toggle);
            } else {
              return _SoundButton.unmute(_toggle);
            }
          }
          return const SizedBox();
        });
  }

  void _toggle() {
    controller.toggleMute();
    setState(() {});
  }
}

class _SoundButton extends IconButton {
  static const EdgeInsets _padding = EdgeInsets.symmetric(horizontal: 8);

  const _SoundButton.mute([VoidCallback? onPressed])
      : super(
            icon: const Icon(Icons.volume_off),
            constraints: const BoxConstraints(),
            padding: _padding,
            onPressed: onPressed,
            tooltip: onPressed == null ? 'No audio' : null);

  const _SoundButton.unmute([VoidCallback? onPressed])
      : super(
            icon: const Icon(Icons.volume_up),
            constraints: const BoxConstraints(),
            padding: _padding,
            onPressed: onPressed,
            tooltip: onPressed == null ? 'No audio' : null);
}
