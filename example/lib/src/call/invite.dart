import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_webrtc_player/flutter_webrtc_player.dart';
import 'package:flutter_webrtc_player_example/src/call/bloc.dart';

class RTCInvite extends StatelessWidget {
  const RTCInvite();

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<RTCPlayerState>(
        stream: context.read<CallCubit>().controller.state,
        builder: (context, snapshot) {
          final state = snapshot.data;
          if (state is PlayingRTCState) {
            return _ByeButton(context.read<CallCubit>().bye);
          }
          return _InviteButton(() {
            showDialog(
                barrierDismissible: false,
                context: context,
                builder: (_) => const OptionsDialog()).then((value) {
              if (value != null) {
                context.read<CallCubit>().initialize(value[0], value[1]);
              }
            });
          });
        });
  }
}

class OptionsDialog extends StatefulWidget {
  const OptionsDialog({Key? key}) : super(key: key);

  @override
  _OptionsDialogState createState() => _OptionsDialogState();
}

class _OptionsDialogState extends State<OptionsDialog> {
  bool audio = true;
  bool video = true;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        title: Text('Options'),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
          SwitchListTile(
              title: const Text('Audio'),
              value: audio,
              onChanged: (value) {
                setState(() {
                  audio = value;
                });
              }),
          SwitchListTile(
              title: const Text('Video'),
              value: video,
              onChanged: (value) {
                setState(() {
                  video = value;
                });
              })
        ]),
        actions: [
          TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: const Text('Cancel')),
          ElevatedButton(
              onPressed: !(audio || video)
                  ? null
                  : () {
                      Navigator.of(context).pop([audio, video]);
                    },
              child: const Text('Start'))
        ]);
  }
}

class _InviteButton extends IconButton {
  static const EdgeInsets _padding = EdgeInsets.symmetric(horizontal: 8);

  _InviteButton(VoidCallback onPressed)
      : super(
            icon: const Icon(Icons.person_add),
            constraints: const BoxConstraints(),
            padding: _padding,
            onPressed: onPressed);
}

class _ByeButton extends IconButton {
  static const EdgeInsets _padding = EdgeInsets.symmetric(horizontal: 8);

  _ByeButton(VoidCallback onPressed)
      : super(
            icon: const Icon(Icons.stop),
            constraints: const BoxConstraints(),
            padding: _padding,
            onPressed: onPressed);
}
