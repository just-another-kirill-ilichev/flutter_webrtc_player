import 'dart:async';
import 'package:flutter_webrtc_player/flutter_webrtc_player.dart';
import 'package:flutter_webrtc_player_example/src/bloc.dart';
import 'package:objectid/objectid.dart';

abstract class CallState {
  const CallState();
}

class CallIdState extends CallState {
  final String id;

  const CallIdState(this.id);
}

class CallInitState extends CallState {
  const CallInitState();
}

class CallLoadingState extends CallState {
  const CallLoadingState();
}

class CallCubit extends BasePlayerCubit<CallState> {
  final CallRTCPlayerController _playerController = CallRTCPlayerController();

  @override
  CallRTCPlayerController get controller => _playerController;

  CallCubit() : super(const CallInitState());

  void initialize(bool audio, bool video) async {
    final String id = ObjectId().hexString;
    emit(const CallLoadingState());
    late StreamSubscription<RTCPlayerState> sub;
    sub = _playerController.state.listen((event) {
      if (event is PlayingRTCState) {
        emit(CallIdState(id));
        sub.cancel();
      }
    });
    await _playerController.connect('wss://api.fastocloud.com/webrtc_in', id);
    _playerController.initialize(audio, video, 1280, 720);
  }

  void bye() {
    _playerController.bye();
  }
}
