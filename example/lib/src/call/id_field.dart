import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_common/widgets.dart';
import 'package:flutter_webrtc_player_example/src/call/bloc.dart';

class CallIdField extends StatelessWidget {
  const CallIdField();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CallCubit, CallState>(builder: (_, state) {
      if (state is CallIdState) {
        return TextFieldEx.readOnly(init: state.id, hint: 'Stream id');
      } else {
        return const SizedBox();
      }
    });
  }
}
