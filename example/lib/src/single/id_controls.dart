import 'package:clipboard/clipboard.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_webrtc_player_example/src/playlist/bloc.dart';

class IdControls extends StatelessWidget {
  const IdControls();

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      BlocBuilder<PlaylistCubit, PlayerIdState>(builder: (_, state) {
        return IconButton(
            icon: const Icon(Icons.paste),
            onPressed: state is PlayerLoadingState
                ? null
                : () {
                    FlutterClipboard.paste().then(context.read<PlaylistCubit>().setId);
                  });
      }),
      IconButton(
          icon: const Icon(Icons.clear),
          onPressed: () {
            context.read<PlaylistCubit>().setId('');
          })
    ]);
  }
}
