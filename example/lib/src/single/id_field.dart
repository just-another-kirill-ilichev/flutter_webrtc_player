import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_webrtc_player_example/src/playlist/bloc.dart';

class IdField extends StatelessWidget {
  const IdField();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PlaylistCubit, PlayerIdState>(builder: (_, state) {
      return TextFieldEx(
          key: ValueKey<String>(state.id),
          init: state.id,
          readOnly: state is PlayerLoadingState,
          hint: 'Stream id',
          onSubmit: context.read<PlaylistCubit>().setId);
    });
  }
}

class TextFieldEx extends TextFormField {
  TextFieldEx(
      {Key? key,
      required String init,
      required String hint,
      required bool readOnly,
      required void Function(String term) onSubmit})
      : super(
            key: key,
            initialValue: init,
            readOnly: readOnly,
            decoration: InputDecoration(hintText: hint, labelText: hint),
            onFieldSubmitted: onSubmit,
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Please enter some text';
              }
              return null;
            });
}
