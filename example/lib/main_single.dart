import 'dart:async';
import 'dart:core';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_webrtc_player_example/src/fullscreen.dart';
import 'package:flutter_webrtc_player_example/src/player.dart';
import 'package:flutter_webrtc_player_example/src/playlist/bloc.dart';
import 'package:flutter_webrtc_player_example/src/single/id_controls.dart';
import 'package:flutter_webrtc_player_example/src/single/id_field.dart';
import 'package:flutter_webrtc_player_example/src/sound.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (_) => PlaylistCubit(),
        child: MaterialApp(theme: ThemeData.dark(), home: Scaffold(body: Center(child: Layout()))));
  }
}

class Layout extends StatefulWidget {
  const Layout({Key? key}) : super(key: key);

  @override
  _LayoutState createState() => _LayoutState();
}

class _LayoutState extends State<Layout> {
  static const Duration TIMEOUT = const Duration(seconds: 5);
  late Timer _timer;
  bool _showControls = true;

  @override
  void initState() {
    super.initState();
    _timer = Timer(TIMEOUT, initTimer);
  }

  @override
  void dispose() {
    _timer.cancel();
    Fullscreen().dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
        onHover: (_) {
          if (!_showControls) {
            setState(() {
              _showControls = true;
            });
          }
          initTimer();
        },
        child: Stack(children: [
          Center(child: Player(context.read<PlaylistCubit>().controller)),
          Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: AnimatedOpacity(
                  opacity: _showControls ? 1.0 : 0.0,
                  duration: const Duration(milliseconds: 100),
                  child: IgnorePointer(
                      ignoring: !_showControls,
                      child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(children: [
                            const SizedBox(width: 8),
                            Expanded(child: IdField()),
                            RTCPlayerSound(context.read<PlaylistCubit>().controller),
                            FullscreenButton(),
                            IdControls()
                          ])))))
        ]));
  }

  void initTimer() {
    _timer.cancel();
    _timer = Timer(TIMEOUT, () {
      setState(() {
        _showControls = false;
      });
    });
  }
}
