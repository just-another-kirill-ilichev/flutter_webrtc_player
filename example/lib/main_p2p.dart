import 'dart:core';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_webrtc_player_example/src/fullscreen.dart';
import 'package:flutter_webrtc_player_example/src/p2p/bloc.dart';
import 'package:flutter_webrtc_player_example/src/p2p/enter_leave.dart';
import 'package:flutter_webrtc_player_example/src/p2p/id_field.dart';
import 'package:flutter_webrtc_player_example/src/p2p/invite.dart';
import 'package:flutter_webrtc_player_example/src/player.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (_) => P2PCallCubit(),
        child: MaterialApp(theme: ThemeData.dark(), home: Scaffold(body: Center(child: Layout()))));
  }
}

class Layout extends StatefulWidget {
  const Layout({Key? key}) : super(key: key);

  @override
  _LayoutState createState() {
    return _LayoutState();
  }
}

class _LayoutState extends State<Layout> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    Fullscreen().dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final controller = context.read<P2PCallCubit>().controller;
    final controllers = context.read<P2PCallCubit>().controllers;
    final controls = Row(children: const [
      Expanded(child: CallIdField()),
      RTCInvite(),
      EnterLeaveSession(),
      FullscreenButton()
    ]);

    return BlocBuilder<P2PCallCubit, CallState>(builder: (_, state) {
      final self = Stack(children: [
        Center(child: Player(controller)),
        Positioned(bottom: 0, left: 0, right: 0, child: controls)
      ]);
      if (controllers.isNotEmpty) {
        List<Widget> players = [];
        for (int i = 0; i < controllers.length; ++i) {
          players.add(Player(controllers[i]));
        }
        final other = Row(children: players);
        return Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [Expanded(flex: 7, child: self), Expanded(flex: 3, child: other)]);
      }

      return self;
    });
  }
}
