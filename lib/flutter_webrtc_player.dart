library flutter_webrtc_player;

export 'package:flutter_webrtc/flutter_webrtc.dart' hide SignalingStateCallback;

export 'src/player/webrtc_controller.dart';
export 'src/player/webrtc_player.dart';
