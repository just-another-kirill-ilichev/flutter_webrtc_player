import 'dart:async';

import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'package:flutter_webrtc_player/src/signaling/signaling.dart';
import 'package:rxdart/rxdart.dart';

abstract class RTCPlayerState {
  const RTCPlayerState();
}

class InitRTCState extends RTCPlayerState {
  const InitRTCState();
}

class ConnectingRTCState extends RTCPlayerState {
  const ConnectingRTCState();
}

class LoadingRTCState extends RTCPlayerState {
  const LoadingRTCState();
}

class ReadyRTCState extends RTCPlayerState {
  const ReadyRTCState();
}

class PlayingRTCState extends RTCPlayerState {
  const PlayingRTCState();
}

typedef void MediaStreamChangedCallback(MediaStream? stream);

abstract class RTCPlayerController<S extends Signaling> {
  MediaStreamChangedCallback? onChangedStream;

  final BehaviorSubject<RTCPlayerState> _state = BehaviorSubject<RTCPlayerState>();
  final RTCVideoRenderer renderer = RTCVideoRenderer();

  S? _signaling;
  Session? _session;
  String? _id;

  Stream<RTCPlayerState> get state => _state.stream;

  MediaStream? get mediaStream => renderer.srcObject;

  String? get id => _id;

  void connect(String url, String sid, [Map<String, dynamic>? iceServers]);

  void init() {
    _state.add(InitRTCState());
    renderer.initialize();
  }

  bool get hasAudio {
    if (mediaStream == null) {
      throw 'Source is not initialized';
    }
    return mediaStream!.getAudioTracks().isNotEmpty;
  }

  bool get muted {
    if (mediaStream == null) {
      throw 'Source is not initialized';
    }
    final tracks = mediaStream!.getAudioTracks();
    if (tracks.isNotEmpty) {
      return !tracks[0].enabled;
    } else {
      throw 'No audio tracks';
    }
  }

  void toggleMute() {
    if (mediaStream == null) {
      throw 'Source is not initialized';
    }
    final tracks = mediaStream!.getAudioTracks();
    if (tracks.isNotEmpty) {
      bool enabled = tracks[0].enabled;
      tracks[0].enabled = !enabled;
    } else {
      throw 'No audio tracks';
    }
  }

  void setMute(bool value) {
    if (mediaStream == null) {
      throw 'Source is not initialized';
    }
    final tracks = mediaStream!.getAudioTracks();
    if (tracks.isNotEmpty) {
      tracks[0].enabled = value;
    } else {
      throw 'No audio tracks';
    }
  }

  void bye() {
    _signaling?.bye(_session?.sid);
    _state.add(InitRTCState());
  }

  void dispose() {
    bye();
    _signaling?.close();
    renderer.dispose();
    _state.close();
  }
}

class PlaylistRTCPlayerController extends RTCPlayerController<SignalingPlaylist> {
  @override
  Future<void> connect(String url, String sid, [Map<String, dynamic>? iceServers]) async {
    _id = sid;
    if (_signaling != null) {
      _signaling!.close();
    }
    _signaling = SignalingPlaylist(sid, iceServers)
      ..onSignalingStateChange = (SignalingState state) {
        switch (state) {
          case SignalingState.ConnectionClosed:
          case SignalingState.ConnectionError:
          case SignalingState.ConnectionOpen:
            break;
        }
      }
      ..onCallStateChange = (Session? session, CallState state) {
        switch (state) {
          case CallState.CallStateNew:
            // renderer.srcObject = null;
            _state.add(LoadingRTCState());
            _session = session;
            break;
          case CallState.CallStateBye:
            renderer.srcObject = null;
            _state.add(LoadingRTCState());
            onChangedStream?.call(null);
            _session = null;
            break;
          case CallState.CallStateConnected:
            _state.add(PlayingRTCState());
            break;
          case CallState.CallStateInvite:
          case CallState.CallStateRinging:
        }
      }
      ..onAddRemoteStream = ((_, stream) {
        renderer.srcObject = stream;
        _state.add(ReadyRTCState());
        onChangedStream?.call(stream);
      })
      ..onRemoveRemoteStream = ((_, stream) {
        renderer.srcObject = null;
        _state.add(LoadingRTCState());
        onChangedStream?.call(null);
      });

    _state.add(ConnectingRTCState());
    return _signaling!.connect(url);
  }
}

class CallRTCPlayerController extends RTCPlayerController<SignalingCall> {
  CallRTCPlayerController({this.onEnterClient, this.onLeaveClient, this.onSessionInfo});

  EnterClientCallback? onEnterClient;
  LeaveClientCallback? onLeaveClient;
  SessionInfoCallback? onSessionInfo;

  @override
  Future<void> connect(String url, String sid, [Map<String, dynamic>? iceServers]) async {
    if (_signaling != null) {
      _signaling!.close();
    }
    _signaling = SignalingCall(sid, iceServers)
      ..onSignalingStateChange = (SignalingState state) {
        switch (state) {
          case SignalingState.ConnectionClosed:
          case SignalingState.ConnectionError:
          case SignalingState.ConnectionOpen:
            break;
        }
      }
      ..onCallStateChange = (Session? session, CallState state) {
        switch (state) {
          case CallState.CallStateNew:
            renderer.srcObject = null;
            _state.add(LoadingRTCState());
            _session = session;
            break;
          case CallState.CallStateBye:
            renderer.srcObject = null;
            _state.add(LoadingRTCState());
            onChangedStream?.call(null);
            _session = null;
            break;
          case CallState.CallStateConnected:
            _state.add(PlayingRTCState());
            break;
          case CallState.CallStateInvite:
          case CallState.CallStateRinging:
        }
      }
      ..onLocalStream = ((_, stream) {
        renderer.srcObject = stream;
        _state.add(ReadyRTCState());
        onChangedStream?.call(stream);
      })
      // proxy callbacks
      ..onEnterClient = ((String ssid, String sid) {
        onEnterClient?.call(ssid, sid);
      })
      ..onLeaveClient = ((String ssid, String sid) {
        onLeaveClient?.call(ssid, sid);
      })
      ..onSessionInfo = ((String ssid, List<String> sids) {
        onSessionInfo?.call(ssid, sids);
      });

    _state.add(ConnectingRTCState());
    return _signaling!.connect(url);
  }

  void initialize(bool audio, bool video, int width, int height) {
    _signaling?.invite(audio, video, width, height);
  }

  void enterSession(String ssid) {
    _signaling?.enterSession(ssid);
  }

  void leaveSession(String ssid) {
    _signaling?.leaveSession(ssid);
  }

  void getSessionInfo(String ssid) {
    _signaling?.getSessionInfo(ssid);
  }
}
