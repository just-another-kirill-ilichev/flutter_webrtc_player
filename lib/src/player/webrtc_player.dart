import 'dart:core';

import 'package:flutter/material.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'package:flutter_webrtc_player/src/player/webrtc_controller.dart';

class WebRTCPlayer extends StatefulWidget {
  final RTCPlayerController controller;
  final Widget placeholder;

  WebRTCPlayer({required this.controller, this.placeholder = const SizedBox()});

  @override
  _WebRTCPlayerState createState() => _WebRTCPlayerState();
}

class _WebRTCPlayerState extends State<WebRTCPlayer> {
  @override
  Widget build(BuildContext context) {
    final builder = StreamBuilder<RTCPlayerState>(
        stream: widget.controller.state,
        builder: (context, snapshot) {
          if (snapshot.data is PlayingRTCState) {
            return RTCVideoView(widget.controller.renderer);
          } else if (snapshot.data is ReadyRTCState) {
            return Center(child: CircularProgressIndicator(color: Colors.green));
          } else if (snapshot.data is LoadingRTCState) {
            return Center(child: CircularProgressIndicator(color: Colors.yellow));
          } else if (snapshot.data is ConnectingRTCState) {
            return Center(child: CircularProgressIndicator(color: Colors.red));
          }
          return widget.placeholder;
        });
    return AspectRatio(aspectRatio: 16 / 9, child: Container(color: Colors.black, child: builder));
  }
}
