// ignore: avoid_web_libraries_in_flutter
import 'dart:html';

typedef void OnMessageCallback(dynamic msg);
typedef void OnCloseCallback(int code, String reason);
typedef void OnOpenCallback();
typedef void OnErrorCallback(String error);

class SimpleWebSocket {
  String _url;
  var _socket;
  OnOpenCallback? onOpen;
  OnMessageCallback? onMessage;
  OnCloseCallback? onClose;
  OnErrorCallback? onError;

  SimpleWebSocket(this._url);

  Future<void> connect() async {
    try {
      _socket = WebSocket(_url);
      _socket.onOpen.listen((e) {
        onOpen?.call();
      });

      _socket.onMessage.listen((e) {
        onMessage?.call(e.data);
      });

      _socket.onError.listen((e) {
        onError?.call(e.data);
      });

      _socket.onClose.listen((e) {
        onClose?.call(e.code, e.reason);
      });
    } catch (e) {
      onClose?.call(500, e.toString());
    }
  }

  void send(data) {
    if (_socket != null) {
      _socket.send(data);
    }
  }

  void close() {
    if (_socket != null) {
      _socket.close();
    }
  }
}
