import 'dart:io';

typedef void OnMessageCallback(dynamic msg);
typedef void OnCloseCallback(int code, String reason);
typedef void OnOpenCallback();
typedef void OnErrorCallback(String error);

class SimpleWebSocket {
  String _url;
  var _socket;
  OnOpenCallback? onOpen;
  OnMessageCallback? onMessage;
  OnCloseCallback? onClose;
  OnErrorCallback? onError;

  SimpleWebSocket(this._url);

  Future<void> connect() async {
    try {
      _socket = await WebSocket.connect(_url);
      onOpen?.call();
      _socket.listen((data) {
        onMessage?.call(data);
      }, onDone: () {
        onClose?.call(_socket.closeCode, _socket.closeReason);
      }, onError: (error) {
        onError?.call(error);
      });
    } catch (e) {
      onClose?.call(500, e.toString());
    }
  }

  void send(data) {
    if (_socket != null) {
      _socket.add(data);
    }
  }

  void close() {
    if (_socket != null) {
      _socket.close();
    }
  }
}
