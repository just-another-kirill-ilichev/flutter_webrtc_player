// ignore: avoid_web_libraries_in_flutter
import 'dart:html' as HTML;

class DeviceInfo {
  static String get label {
    return 'FastoCloud ( ' + HTML.window.navigator.userAgent + ' )';
  }

  static String get userAgent {
    return 'fastocloud/web-plugin 0.0.1';
  }
}
