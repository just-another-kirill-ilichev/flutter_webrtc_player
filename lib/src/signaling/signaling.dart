library signaling;

import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'package:flutter_webrtc_player/src/utils/random_string.dart';

import '../utils/device_info.dart' if (dart.library.js) '../utils/device_info_web.dart';
import '../utils/websocket.dart' if (dart.library.js) '../utils/websocket_web.dart';

part 'signaling_call.dart';

part 'signaling_playlist.dart';

const String kFrom = 'from';
const String kTo = 'to';
const String kSessionID = 'session_id';

// comands
const String kNewCommand = 'new';
const String kOfferCommand = 'offer';
const String kAnswerCommand = 'answer';
const String kCandidateCommand = 'candidate';
const String kByeCommand = 'bye';

enum SignalingState {
  ConnectionOpen,
  ConnectionClosed,
  ConnectionError,
}

enum CallState {
  CallStateNew,
  CallStateRinging,
  CallStateInvite,
  CallStateConnected,
  CallStateBye,
}

/*
 * callbacks for Signaling API.
 */
typedef void SignalingStateCallback(SignalingState state);
typedef void CallStateCallback(Session? session, CallState state);
typedef void StreamStateCallback(Session session, MediaStream stream);
typedef void OtherEventCallback(dynamic event);
typedef void LogsEventCallback(String event);

class Session {
  Session({required this.sid, required this.pid});

  String pid;
  String sid;
  RTCPeerConnection? pc;
  List<RTCIceCandidate> remoteCandidates = [];
}

abstract class Signaling {
  Signaling(this.streamId, [Map<String, dynamic>? iceServers]) {
    if (iceServers != null) {
      _iceServers = iceServers;
    }
  }

  String streamId;

  JsonEncoder _encoder = JsonEncoder();
  JsonDecoder _decoder = JsonDecoder();
  String _selfId = randomNumeric(8);
  late SimpleWebSocket _socket;
  Map<String, Session> _sessions = {};

  SignalingStateCallback? onSignalingStateChange;
  CallStateCallback? onCallStateChange;

  String get sdpSemantics => WebRTC.platformIsWindows ? 'plan-b' : 'unified-plan';

  Map<String, dynamic> _iceServers = {
    'iceServers': [
      // {'urls': 'stun:stun.l.google.com:19302'},
      // {'urls': 'stun:us-turn10.xirsys.com'},
      // {'urls': 'turn:turn.ivrata.com', 'credential': 'password123', 'username': 'turn'},

      {'urls': 'stun:stun.fastocloud.com:3478'},
      {
        'urls': 'turn:turn.fastocloud.com:5349',
        'credential': 'fastocloud',
        'username': 'fastocloud'
      },
    ]
  };

  void close() async {
    await _cleanSessions();
    _socket.close();
  }

  void bye(String? sessionId) {
    if (sessionId == null) {
      return;
    }

    _send(kByeCommand, {
      kTo: sessionId,
      kFrom: _selfId,
    });

    _closeSession(_sessions[sessionId]);
  }

  void onMessage(message);

  Future<void> connect(String url) async {
    _socket = SimpleWebSocket(url);
    _socket.onOpen = () {
      print('onOpen');
      onSignalingStateChange?.call(SignalingState.ConnectionOpen);
      _send(kNewCommand, {
        kFrom: _selfId,
        kTo: streamId,
        'name': DeviceInfo.label,
        'user_agent': DeviceInfo.userAgent
      });
    };

    _socket.onMessage = (message) {
      final converted = _decoder.convert(message);
      print('[RECEIVE]: $message');
      onMessage(converted);
    };

    _socket.onError = (error) {
      print('Error: ' + error);
      onSignalingStateChange?.call(SignalingState.ConnectionError);
    };

    _socket.onClose = (int code, String reason) {
      print('Closed by server [$code => $reason]!');
      onSignalingStateChange?.call(SignalingState.ConnectionClosed);
    };

    await _socket.connect();
  }

  @protected
  void onSdpSemantics(Session session, RTCPeerConnection pc);

  Future<RTCPeerConnection> _initSession(Session session, Map<String, dynamic> config) async {
    final Map<String, dynamic> args = {
      ..._iceServers,
      ...{'sdpSemantics': sdpSemantics}
    };
    print('initSession: ' + args.toString());
    RTCPeerConnection pc = await createPeerConnection(args, config);

    onSdpSemantics(session, pc);

    pc.onIceCandidate = (RTCIceCandidate candidate) {
      _send(kCandidateCommand, {
        kFrom: _selfId,
        kTo: streamId,
        'sdpMLineIndex': candidate.sdpMlineIndex,
        'sdpMid': candidate.sdpMid,
        'candidate': candidate.candidate
      });
    };

    pc.onIceConnectionState = (state) {
      print('onIceConnectionState: ' + state.toString());
      if (state == RTCIceConnectionState.RTCIceConnectionStateConnected) {
        onCallStateChange?.call(session, CallState.CallStateConnected);
      }
    };

    return pc;
  }

  void _send(event, data) {
    var request = Map();
    request["type"] = event;
    request["data"] = data;
    _socket.send(_encoder.convert(request));
    print('[SEND]: $event: $data');
  }

  Future<void> _cleanSessions() async {
    _sessions.forEach((key, sess) async {
      await sess.pc?.close();
    });
    _sessions.clear();
  }

  Future<void> _closeSession(Session? session) async {
    await session?.pc?.close();
  }
}
