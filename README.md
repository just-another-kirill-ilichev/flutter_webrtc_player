# flutter_webrtc_player

FastoCloud based Flutter WebRTC plugin

Depends on:
- [Flutter WebRTC](https://github.com/flutter-webrtc/flutter-webrtc)
- [FastoCloud API Backend](https://gitlab.com/fastogt/gofastocloud)

Tested on:
- Web browsers (Edge, Chrome, Chromium, FireFox)
- Android

How to use:
- You need to setup [FastoCloud PRO](https://gitlab.com/fastogt/fastocloud_pro) media part
- Setup [FastoCloud API Backend](https://gitlab.com/fastogt/gofastocloud)
- Activate API backend and Media part
- Integrate flutter_webrtc_player plugin into your app, connect to websocket with Stream ID (Stream should be in playing state, output unknown://fake)
